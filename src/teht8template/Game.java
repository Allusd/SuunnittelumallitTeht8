/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht8template;

/**
 *
 * @author aleks
 */
abstract class Game {
    

    abstract void initializeGame();
    abstract void makePlay();


    
    public final void playOneGame(){

        initializeGame();

        makePlay();
    }
    
}
